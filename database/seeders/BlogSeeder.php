<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('blogs')->insert([
            [
                'title' => 'title blog sample 2',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vitae massa nec erat semper mattis a a nulla. Duis quis turpis ultrices, tincidunt urna et, tincidunt dui. Nulla facilisi. Nullam felis risus, tincidunt vel lacinia non, dignissim sed mauris. Integer et purus eget magna egestas lacinia. Nam nunc tellus, mattis sollicitudin facilisis non, rhoncus nec risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc a mollis magna. In at rutrum diam, eget commodo ante. Maecenas quis erat libero. Sed rutrum finibus massa, eu ultricies leo dictum et. Morbi venenatis pellentesque metus, sed tincidunt tellus. Etiam sit amet dolor arcu.',
            ],
            [
                'title' => 'title blog sample 3',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vitae massa nec erat semper mattis a a nulla. Duis quis turpis ultrices, tincidunt urna et, tincidunt dui. Nulla facilisi. Nullam felis risus, tincidunt vel lacinia non, dignissim sed mauris. Integer et purus eget magna egestas lacinia. Nam nunc tellus, mattis sollicitudin facilisis non, rhoncus nec risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc a mollis magna. In at rutrum diam, eget commodo ante. Maecenas quis erat libero. Sed rutrum finibus massa, eu ultricies leo dictum et. Morbi venenatis pellentesque metus, sed tincidunt tellus. Etiam sit amet dolor arcu.',
            ],
    ]);
        
    }
}
