<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Comment;
class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Comment::create(
            [
                'nama'=>'Eri',
                'comment'=>'blog keren',
                'blog_id'=>4
            ]
            );
        Comment::create(
        [
            'nama'=>'Akbar',
            'comment'=>'blog mantap',
            'blog_id'=>4
        ]);
    }
}
