<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Blog;
use App\Models\Comment;
use Inertia\Inertia;
class BlogController extends Controller
{
    public function index()  {
        $blog = Blog::where('id','>=','4')->orderBy('title','desc')->get();
        
        return Inertia::render('Blog',[
            'blog'=>$blog,
        ]);
    }
    public function getComment(){
        $comment = Comment::all();
        foreach ($comment as $item) {
            print($item->blog->id);
        }
    }
    public function show(string $id):View  {
        $blog = Blog::find($id);
        return view("blog.show",["id"=>$id,'blog'=>$blog]);
    }
    public function edit(string $id)  {
        $blog = Blog::find($id);
        return Inertia::render('Blog/BlogEdit',[
            'blog'=>$blog,
        ]);
    }
    public function store(Request $request) {
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->save();
        return redirect('blog');
    }
    public function create()  {
        return Inertia::render('Blog/BlogCreate');
    }
    public function update(Request $request){
        // dd($request->all());
        $blog = Blog::find($request->id);
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->save();
        return redirect('blog');
    }
    public function delete(Request $request){
        $blog = Blog::find($request->id);
        $blog->delete();
        return redirect('blog');
    }
    public function comment(Request $request) {
        $comment = new Comment;
        $comment->nama = $request->nama;
        $comment->comment = $request->comment;
        $comment->blog_id = $request->id;
        $comment->save();
        return redirect()->back();
    }
}
