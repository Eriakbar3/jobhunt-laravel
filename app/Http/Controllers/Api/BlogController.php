<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\User;
use Response;
class BlogController extends Controller
{
    public function index(){
        $blog = Blog::with('comments')->get();
        return Response::json($blog);
    }
    public function getUser(){
        $user = User::with('blog')->get();
        return Response::json($user);
    }
    public function getComment(){
        $comment = Comment::with('blog')->get();
        return Response::json($comment);
    }
    public function show(Request $request,$id){
        $blog = Blog::orderBy('title','asc')->take(2)->get();
        return Response::json($blog);
    }
    public function store(Request $request){
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->save();
        return Response::json(["status"=>200,"data"=>$blog]);
    }
    public function update(Request $request){
        $blog = Blog::find($request->id);
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->save();
        return Response::json(["status"=>200,"data"=>$blog]);
    }
    public function delete(Request $request){
        $blog = Blog::find($request->id);
        $blog->delete();
        return Response::json(["status"=>200,"message"=>"id $request->id is deleted"]);
    }
}
