<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Blog</title>
</head>
<body>
    <form action="{{url('blog/update')}}" method="post">
        <input type="hidden" name="id" value="{{$blog->id}}">
        @csrf
        <div>
            <label>Title</label>
            <input type="text" name="title" id="" value="{{$blog->title}}">
        </div>
        <div>
            <label>Content</label>
            <textarea name="content" id="" cols="30" rows="10">{{$blog->content}}</textarea>
        </div>
        <input type="submit" value="simpan">
    </form>
</body>
</html>