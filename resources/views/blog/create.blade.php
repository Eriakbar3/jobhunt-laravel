<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
</head>
<body>
    <form action="{{url('blog')}}" method="post">
        @csrf
        <div>
            <label>Title</label>
            <input type="text" name="title" id="">
        </div>
        <div>
            <label>Content</label>
            <textarea name="content" id="" cols="30" rows="10"></textarea>
        </div>
        <input type="submit" value="simpan">
    </form>
</body>
</html>