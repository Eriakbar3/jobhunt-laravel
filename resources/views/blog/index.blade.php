<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
</head>
<body>
    <h1>Blog</h1>
    @foreach($blog as $item)
        <h2><a href="{{url('blog/'.$item->id)}}"> {{$item->title}}</a></h2>
        <p> {{$item->content}} </p>
        <a href="{{url('blog/edit/'.$item->id)}}">Edit</a>
        <a href="#" onclick="deleteForm({{$item->id}})">Delete</a>
        <form action="{{url('blog/delete')}}" method="post" id="form-{{$item->id}}">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}">
        </form>
        @foreach($item->comments as $comment)
            <h5>{{$comment->nama}}</h5>
            <p>{{$comment->comment}}</p>
        @endforeach
        <hr>

    @endforeach

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function deleteForm(id) {
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete'
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $('#form-'+id).submit();
                }
            })
            
        }
    </script>
</body>
</html>