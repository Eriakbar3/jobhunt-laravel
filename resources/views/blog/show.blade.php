<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>{{$blog->title}}</h2>
    <p>{{$blog->content}}</p>
    <h3>Comment</h3>
    @foreach($blog->comments as $comment)
            <h5>{{$comment->nama}}</h5>
            <p>{{$comment->comment}}</p>
    @endforeach
    <form action="{{url('comment')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$blog->id}}">
        <div style="width:100%;margin:10px 0px">
        <input type="text" name="nama" id="" placeholder="masukan nama" style="width:100px">
        </div>
        <div style="width:100%;margin:10px 0px">
        <textarea name="comment" id="" cols="30" rows="4"></textarea>
        </div>
        <input type="submit" value="add comment">
    </form>
</body>
</html>